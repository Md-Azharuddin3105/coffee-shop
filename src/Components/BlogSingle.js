import React, { Component } from 'react'
import Footer from './Footer'
import Header from './Header'

export class BlogSingle extends Component {
    render() {
        return (
            <div>
                <Header />
                <div class="page-title-area text-center" style="background-image: url('./assets/img/bg/19.png')">
                    <div class="container">
                        <div class="breadcrumb-inner">
                            <h2 class="page-title">Blog</h2>
                        </div>
                    </div>
                </div>
                {/* page title end */}

                {/* start blog page */}
                <div class="main-blog-area pd-top-150 pd-bottom-150">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7 col-12">
                                <div class="single-blog-card">
                                    <div class="thumb">
                                        <img src="assets/img/blog/4.png" alt="blog" />
                                        <span class="date">29 March, 2020</span>
                                    </div>
                                    <div class="blog-content">
                                        <h4><a href="blog-single.html">World’s most famous coffee and pastry</a></h4>
                                        <p>Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod. Pellentesque dignissim volutpat orci</p>
                                    </div>
                                </div>
                                <div class="single-blog-card">
                                    <div class="thumb">
                                        <img src="assets/img/blog/5.png" alt="blog" />
                                        <a href="https://www.youtube.com/watch?v=c7XEhXZ_rsk" class="video-play-btn mfp-iframe"><i class="fa fa-play"></i></a>
                                        <span class="date">29 March, 2020</span>
                                    </div>
                                    <div class="blog-content">
                                        <h4><a href="blog-single.html">How to make a great coffee</a></h4>
                                        <p>Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod. Pellentesque dignissim volutpat orci</p>
                                    </div>
                                </div>
                                <div class="single-blog-card">
                                    <div class="thumb">
                                        <img src="assets/img/blog/6.png" alt="blog" />
                                        <span class="date">29 March, 2020</span>
                                    </div>
                                    <div class="blog-content">
                                        <h4><a href="blog-details.html">How to make a great coffee</a></h4>
                                        <p>Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod. Pellentesque dignissim volutpat orci</p>
                                    </div>
                                </div>
                                <nav class="td-page-navigation mb-5 mb-lg-0">
                                    <ul class="pagination">
                                        <li class="pagination-arrow"><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a class="active" href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li class="pagination-arrow"><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                                    </ul>
                                </nav>
                            </div>
                            {/* sidebar */}
                            <div class="col-lg-5 col-12">
                                <div class="td-sidebar mt-5 mt-lg-0">
                                    <div class="widget widget_search">
                                        <form class="search-form">
                                            <div class="form-group">
                                                <input type="text" placeholder="Search" />
                                            </div>
                                            <button class="submit-btn" type="submit"><i class="fa fa-search"></i></button>
                                        </form>
                                    </div>                  
                                    <div class="widget widget-recent-post">                            
                                        <h4 class="widget-title">Recent Post</h4>
                                        <ul>
                                            <li>
                                                <div class="media">
                                                    <div class="media-left">
                                                        <img src="assets/img/blog/7.png" alt="blog" />
                                                    </div>
                                                    <div class="media-body">
                                                        <h5 class="title"><a href="#">World’s most famous coffee</a></h5>
                                                        <div class="post-info"><a href="#">Blog</a><span>, 15 March, 2020</span></div>                             
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="media">
                                                    <div class="media-left">
                                                        <img src="assets/img/blog/8.png" alt="blog" />
                                                    </div>
                                                    <div class="media-body">
                                                        <h5 class="title"><a href="#">World’s most delicious pastry,</a></h5>
                                                        <div class="post-info"><a href="#">Blog</a><span>, 15 March, 2020</span></div>                             
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="media">
                                                    <div class="media-left">
                                                        <img src="assets/img/blog/9.png" alt="blog" />
                                                    </div>
                                                    <div class="media-body">
                                                        <h5 class="title"><a href="#">World’s most famous coffee and pastry,</a></h5>
                                                        <div class="post-info"><a href="#">Blog</a><span>, 15 March, 2020</span></div>                             
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="widget widget_catagory">
                                        <h4 class="widget-title">Catagory</h4>                                 
                                        <ul class="catagory-items">
                                            <li><a href="#"><i class="fa fa-circle"></i>Community</a></li>
                                            <li><a href="#"><i class="fa fa-circle"></i>Branding</a></li>
                                            <li><a href="#"><i class="fa fa-circle"></i>Business</a></li>
                                            <li><a href="#"><i class="fa fa-circle"></i>Webdesign</a></li>
                                            <li><a href="#"><i class="fa fa-circle"></i>Development</a></li>
                                            <li><a href="#"><i class="fa fa-circle"></i>Corporate</a></li>
                                            <li><a href="#"><i class="fa fa-circle"></i>News & Trend</a></li>
                                        </ul>
                                    </div> 
                                    <div class="widget widget_twitter">
                                        <h4 class="widget-title">Twitter</h4>                                 
                                        <ul class="catagory-items">
                                            <li><a href="#"><i class="fa fa-twitter"></i>@cbszone ShopStore A complete WordPress ecommerce theme to sell products online medium.com/@cbszone/sho…</a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i>@cbszone ShopStore A complete WordPress ecommerce theme to sell products online medium.com/@cbszone/sho…</a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i>@cbszone ShopStore A complete WordPress ecommerce theme to sell products online medium.com/@cbszone/sho…</a></li>
                                        </ul>
                                    </div>
                                    <div class="widget widget_tags mb-0">
                                        <h4 class="widget-title">Tags</h4>
                                        <div class="tagcloud">
                                            <a href="#">Art</a>
                                            <a href="#">Creative</a>
                                            <a href="#">Article</a>
                                            <a href="#">Designer</a>
                                            <a href="#">Portfolio</a>
                                            <a href="#">Project</a>
                                            <a href="#">Personal</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* sidebar */}
                        </div>
                    </div>
                </div>
                {/* blog area end */}
                <Footer />
                
            </div>
        )
    }
}

export default BlogSingle
