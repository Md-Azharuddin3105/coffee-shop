import React, { Component } from 'react'
import Footer from './Footer'
import Header from './Header'

export class Gallery extends Component {
    render() {
        return (
            <div>
                <Header />
                <div class="page-title-area text-center" style="background-image: url('./assets/img/bg/18.png')">
                    <div class="container">
                        <div class="breadcrumb-inner">
                            <h2 class="page-title">Gallery</h2>
                        </div>
                    </div>
                </div>
                page title end

                {/* gallery area start */}
                <div class="gallery-area pd-top-150 pd-bottom-150">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="single-gallery">
                                    <img src="assets/img/gallery/1.png" alt="img" />
                                </div>
                                <div class="single-gallery mb-md-0">
                                    <img src="assets/img/gallery/2.png" alt="img" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="single-gallery mb-md-0">
                                    <img src="assets/img/gallery/3.png" alt="img" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="single-gallery">
                                    <img src="assets/img/gallery/4.png" alt="img" />
                                </div>
                                <div class="single-gallery">
                                    <img src="assets/img/gallery/5.png" alt="img" />
                                </div>
                                <div class="single-gallery mb-0">
                                    <img src="assets/img/gallery/6.png" alt="img" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* gallery area end */}
                <Footer />
            </div>
        )
    }
}

export default Gallery
