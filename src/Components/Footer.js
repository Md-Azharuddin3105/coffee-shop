import React, { Component } from 'react'

export class Footer extends Component {
    render() {
        return (
            <div>
                <footer class="footer-area bg-black">
                    <div class="container">
                        <div class="footer-top">
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="widget widget-about">
                                        <div class="thumb">
                                            <img src="assets/img/logo.png" alt="img" />
                                        </div>
                                        <ul class="social-media">
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-6">
                                    <div class="widget widget-nav-menu">
                                        <ul>
                                            <li><a href="#">Home</a></li>
                                            <li><a href="#">About Us</a></li>
                                            <li><a href="#">Flavor</a></li>
                                            <li><a href="#">Collections</a></li>
                                            <li><a href="#">Menu</a></li>
                                            <li><a href="#">Our Chefs</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="widget widget-contact">
                                        <p>Cecilia Chapman <br />711-2880 Nulla St.<br />Mankato Mississippi 96522 <br />(257) 563-7401</p>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="widget widget-subscribe">
                                        <p>Sign up To Our Newsletter</p>
                                        <div class="subscribe-inner">
                                            <input type="text" placeholder="Enter Your Email" />
                                            <button><img src="assets/img/icon/5.png" alt="img" /></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="copy-right">
                            <p>@ Caffee"d"Maria 2020 All Right Reserved. Powered With <span><i class="fa fa-heart-o"></i> by CbsZone Themes</span></p>
                        </div>
                    </div>
                </footer>
                {/* footer area end  */}
            </div>
        )
    }
}

export default Footer
