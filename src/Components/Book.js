import React, { Component } from 'react'
import Footer from './Footer'
import Header from './Header'

export class Book extends Component {
    render() {
        return (
            <div>
                <Header />
                <div class="page-title-area style-two text-center" style="background-image: url('./assets/img/bg/20.png')">
                    <div class="container">
                        <div class="breadcrumb-inner">
                            <h2 class="page-title">Book A Table</h2>
                        </div>
                    </div>
                </div>
                {/* page title end */}

                {/* book area start */}
                <div class="book-page-area pd-bottom-150" style="background-image: url('./assets/img/bg/15.png')">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-xl-7 col-lg-9 col-md-11 align-self-center">
                                <form class="contact-form-inner booking-style mg-top--250">
                                    <div class="thumb text-center mb-5 mb-md-5">
                                        <img src="assets/img/icon/14.png" alt="img" />
                                        <h5 class="mt-3">Need a reservation?</h5>
                                    </div>
                                    <div class="single-input-inner">
                                        <input type="text" placeholder="Your Name" />
                                    </div>
                                    <div class="single-input-inner">
                                        <input type="text" placeholder="Phone Number" />
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="single-input-inner">
                                                <select class="single-select">
                                                    <option value="1">CHOICE ONE</option>
                                                    <option value="1">CHOICE ONE</option>
                                                    <option value="1">CHOICE ONE</option>
                                                    <option value="1">CHOICE ONE</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="single-input-inner">
                                                <select class="single-select">
                                                    <option value="1">CHOICE ONE</option>
                                                    <option value="1">CHOICE ONE</option>
                                                    <option value="1">CHOICE ONE</option>
                                                    <option value="1">CHOICE ONE</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-input-inner">
                                        <textarea rows="5" placeholder="Message"></textarea>
                                    </div>
                                    <div class="btn-wrap text-center">
                                        <a class="btn btn-red" href="#">Submit Order</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {/* book area end */}
                <Footer />
            </div>
        )
    }
}

export default Book
