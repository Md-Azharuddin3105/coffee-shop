import React, { Component } from 'react'
import Footer from './Footer'
import Home from './Home' 

export class About extends Component {
    componentDidMount(){
        
    }
    render() {
        return (
            <div>
                {/* preloader area start */}
                    <Home />
                    {/* navbar end */}

                    {/* page title start */}
                    <div class="page-title-area text-center" style="background-image: url('./assets/img/bg/7.png')">
                        <div class="container">
                            <div class="breadcrumb-inner">
                                <h2 class="page-title">About Us</h2>
                            </div>
                        </div>
                    </div>
                    {/* page title end */}

                    {/* about area start */}
                    <div class="about-area pd-top-130 pd-bottom-150">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="thumb mb-5 mb-lg-0">
                                        <img src="assets/img/other/6.png" alt="img" />
                                    </div>
                                </div> 
                                <div class="col-lg-6">
                                    <div class="about-inner style-three text-center">
                                        <h3>Cafe <span>D</span> Maria</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus nibh et justo cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor</p>
                                        <div class="author-info">
                                            <div class="rating">
                                                <span>Ratting :</span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <h5>"Best cafe in London. Ever!”</h5>
                                            <p>“Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus nibh et justo</p>
                                            <span class="designation">Peter Johnson, Chef</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* about area end */}

                    {/* call to action area start */}
                    <div class="call-to-action" style="background-image: url('./assets/img/bg/8.png')">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-7 align-self-center">
                                    <h2>Enjoy your Cafe' D</h2>
                                </div>
                                <div class="col-sm-5">
                                    <div class="btn-wrap text-lg-right mt-4 mt-sm-0">
                                        <a class="btn btn-border-shadow-white mt-3" href="#"><span>View Menu <i class="la la-arrow-right"></i></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* call to action area end */}

                    {/* client area start */}
                    <div class="client-area pd-top-105">
                        <img class="h2-client-img" src="assets/img/bg/12.png" alt="img" />
                        <div class="container">
                            <div class="section-title text-center">
                                <h2 class="title">Happy Client's</h2>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-xl-9 col-lg-11">
                                    <div class="slider client-slider-thumb">
                                        <div class="item">
                                            <div class="thumb">
                                                <img src="assets/img/client/1.png" alt="img" />
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="thumb">
                                                <img src="assets/img/client/2.png" alt="img" />
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="thumb">
                                                <img src="assets/img/client/3.png" alt="img" />
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="thumb">
                                                <img src="assets/img/client/4.png" alt="img" />
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="thumb">
                                                <img src="assets/img/client/5.png" alt="img" />
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="thumb">
                                                <img src="assets/img/client/4.png" alt="img" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-xl-6 col-lg-7">
                                    <div class="slider client-slider-content">
                                        <div class="item">
                                            <div class="details">
                                                <p>Sed sagittis sodales lobortis. Curabitur in eleifend turpis, id vehicula odio. Donec pulvinar tellus egetmagna aliquet ultricies. Praesent gravida hendrerit ex, nec eleifend sem convallis vitae.</p>
                                                <h6>Jhon Doe Smith</h6>
                                                <span>Coffee Director</span>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="details">
                                                <p>Sed sagittis sodales lobortis. Curabitur in eleifend turpis, id vehicula odio. Donec pulvinar tellus egetmagna aliquet ultricies. Praesent gravida hendrerit ex, nec eleifend sem convallis vitae.</p>
                                                <h6>Jhon Doe</h6>
                                                <span>Coffee Director</span>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="details">
                                                <p>Sed sagittis sodales lobortis. Curabitur in eleifend turpis, id vehicula odio. Donec pulvinar tellus egetmagna aliquet ultricies. Praesent gravida hendrerit ex, nec eleifend sem convallis vitae.</p>
                                                <h6>Director Khan</h6>
                                                <span>Coffee Director</span>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="details">
                                                <p>Sed sagittis sodales lobortis. Curabitur in eleifend turpis, id vehicula odio. Donec pulvinar tellus egetmagna aliquet ultricies. Praesent gravida hendrerit ex, nec eleifend sem convallis vitae.</p>
                                                <h6>Smith Jose</h6>
                                                <span>Coffee Director</span>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="details">
                                                <p>Sed sagittis sodales lobortis. Curabitur in eleifend turpis, id vehicula odio. Donec pulvinar tellus egetmagna aliquet ultricies. Praesent gravida hendrerit ex, nec eleifend sem convallis vitae.</p>
                                                <h6>Jhon Khan</h6>
                                                <span>Coffee Director</span>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="details">
                                                <p>Sed sagittis sodales lobortis. Curabitur in eleifend turpis, id vehicula odio. Donec pulvinar tellus egetmagna aliquet ultricies. Praesent gravida hendrerit ex, nec eleifend sem convallis vitae.</p>
                                                <h6>Director Smith</h6>
                                                <span>Coffee Director</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <Footer />
            </div>
        )
    }
}

export default About
