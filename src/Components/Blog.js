import React, { Component } from 'react'
import Footer from './Footer'
import Home from './Home'

// const blogPosts = [
//     {
        
//     }
//     // set your object here and add this object to the state
//     //ok
// ]

export class Blog extends Component {
    state = {
        blogs: [
            {
                imgUrl: 'assets/img/blog/4.png',
                date: '29 March, 2020',
                title: 'World’s most famous coffee and pastry',
                disc: 'Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod. Pellentesque dignissim volutpat orci',
            },{
                imgUrl: 'assets/img/blog/4.png',
                date: '29 March, 2020',
                title: 'World’s most famous coffee and pastry',
                disc: 'Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod. Pellentesque dignissim volutpat orci',
            },{
                imgUrl: 'assets/img/blog/4.png',
                date: '29 March, 2020',
                title: 'World’s most famous coffee and pastry',
                disc: 'Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod. Pellentesque dignissim volutpat orci',
            }],
        posts: [
            {
                imaUrl: 'assets/img/blog/7.png',
                title: 'World’s most famous coffee',
                date: '15 March, 2020'
            }
        ]
    }
    render() {
        return (
            <div>
                <Home />
                {/* page title start */}
                    <div class="page-title-area text-center" style="background-image: url('./assets/img/bg/19.png')">
                        <div class="container">
                            <div class="breadcrumb-inner">
                                <h2 class="page-title">Blog</h2>
                            </div>
                        </div>
                    </div>
                    {/* page title end */}

                    {/* start blog page */}
                    <div class="main-blog-area pd-top-150 pd-bottom-150">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-7 col-12">
                                    {this.state.blogs.map((blog) => {
                                        return (
                                            <div class="single-blog-card">
                                                <div class="thumb">
                                                    <img src={blog.imgUrl} alt="blog" />
                                                    <span class="date">{blog.date}</span>
                                                </div>
                                                <div class="blog-content">
                                                    <h4><a href="blog-single.html">{blog.title}</a></h4>
                                                    <p>{blog.disc}</p>
                                                </div>
                                            </div>
                                        )
                                    })}
                                    <nav class="td-page-navigation mb-5 mb-lg-0">
                                        <ul class="pagination">
                                            <li class="pagination-arrow"><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                                            <li><a href="#">1</a></li>
                                            <li><a class="active" href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li class="pagination-arrow"><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                                        </ul>
                                    </nav>
                                </div>
                                {/* sidebar */}
                                <div class="col-lg-5 col-12">
                                    <div class="td-sidebar mt-5 mt-lg-0">
                                        <div class="widget widget_search">
                                            <form class="search-form">
                                                <div class="form-group">
                                                    <input type="text" placeholder="Search" />
                                                </div>
                                                <button class="submit-btn" type="submit"><i class="fa fa-search"></i></button>
                                            </form>
                                        </div>                  
                                        <div class="widget widget-recent-post">                            
                                            <h4 class="widget-title">Recent Post</h4>
                                            <ul>
                                                {this.state.posts.map((post) => {
                                                    return (
                                                        <li>
                                                            <div class="media">
                                                                <div class="media-left">
                                                                    <img src={post.imaUrl} alt="blog" />
                                                                </div>
                                                                <div class="media-body">
                                                                    <h5 class="title"><a href="#">{post.title}</a></h5>
                                                                    <div class="post-info"><a href="#">Blog</a><span>, {post.date}</span></div>                             
                                                                </div>
                                                            </div>
                                                        </li>
                                                    )
                                                })}
                                            </ul>
                                        </div>
                                        <div class="widget widget_catagory">
                                            <h4 class="widget-title">Catagory</h4>                                 
                                            <ul class="catagory-items">
                                                <li><a href="#"><i class="fa fa-circle"></i>Community</a></li>
                                                <li><a href="#"><i class="fa fa-circle"></i>Branding</a></li>
                                                <li><a href="#"><i class="fa fa-circle"></i>Business</a></li>
                                                <li><a href="#"><i class="fa fa-circle"></i>Webdesign</a></li>
                                                <li><a href="#"><i class="fa fa-circle"></i>Development</a></li>
                                                <li><a href="#"><i class="fa fa-circle"></i>Corporate</a></li>
                                                <li><a href="#"><i class="fa fa-circle"></i>News & Trend</a></li>
                                            </ul>
                                        </div> 
                                        <div class="widget widget_twitter">
                                            <h4 class="widget-title">Twitter</h4>                                 
                                            <ul class="catagory-items">
                                                <li><a href="#"><i class="fa fa-twitter"></i>@cbszone ShopStore A complete WordPress ecommerce theme to sell products online medium.com/@cbszone/sho…</a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i>@cbszone ShopStore A complete WordPress ecommerce theme to sell products online medium.com/@cbszone/sho…</a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i>@cbszone ShopStore A complete WordPress ecommerce theme to sell products online medium.com/@cbszone/sho…</a></li>
                                            </ul>
                                        </div>
                                        <div class="widget widget_tags mb-0">
                                            <h4 class="widget-title">Tags</h4>
                                            <div class="tagcloud">
                                                <a href="#">Art</a>
                                                <a href="#">Creative</a>
                                                <a href="#">Article</a>
                                                <a href="#">Designer</a>
                                                <a href="#">Portfolio</a>
                                                <a href="#">Project</a>
                                                <a href="#">Personal</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* sidebar */}
                            </div>
                        </div>
                    </div>
                    {/* blog area end */}
                <Footer />
            </div>
        )
    }
}

export default Blog
