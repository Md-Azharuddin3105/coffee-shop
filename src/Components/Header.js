import React, { Component } from 'react'
import '../assets/css/responsive.css'
import '../assets/css/style.css'
import '../assets/css/vendor.css'

export class Header extends Component {
    componentDidMount(){
        document.querySelector('.navbar-area .navbar-nav li.menu-item-has-children>a').addEventListener('click',(e) => {
            e.preventDefault()
        })
        document.querySelector('.navbar-area .menu').addEventListener('click', function(){
            this.toggleClass('open');
            document.querySelector('.navbar-area .navbar-collapse').toggleClass('sopen')
        })
        if((window).width() < 1992){
            document.querySelector(".in-mobile").cloneNode().appendTo(".sidebar-inner")
            document.querySelector(".in-mobile ul li.menu-item-has-children").append('<i class="fas fa-chevron-right"></i>')
            document.querySelector('<i class="fas fa-chevron-right"></i>').insertAfter("")
            document.querySelector(".menu-item-has-children a").addEventListener('click', function(){
                this.siblings('.sub-menu').animate({
                    height: "toggle"
                }, 300);
            })
        }
        var menutoggle = document.querySelector('.menu-toggle')
        var mainmenu = document.querySelector('.navbar-nav')
        menutoggle.addEventListener('click', function(){
            if(menutoggle.hasclass('is-active')){
                mainmenu.removeClass('menu-open');
            }
            else {
                mainmenu.addClass('menu-open');
            }
        })
        if(document.querySelectorAll('.menu-bar').length){
            document.querySelector(".menu-bar").addEventListener('click', function(){
                document.querySelector(".ba-navbar").toggleClass("ba-navbar-show", "linear")
            })
            document.querySelector('body').addEventListener('click', function(e){
                if(!document.querySelector(e.target).closest('.menu-bar').length && !document.querySelector(e.target).closest('.ba-navbar').length){
                    document.querySelector('.ba-navbar').removeClass('ba-navbar-show')
                }
            })
            document.querySelector(".menu-close").addEventListener('click', function(){
                document.querySelector(".ba-navbar").toggleClass("ba-navbar-show", "linear")
            })
        }
        if(document.querySelectorAll('.single-select').length){
            document.querySelector('.single-select').niceSelect();
        }
        // parallax part is remaining
        // isotope filter part is incomplete
        // var Container = document.querySelectorAll('.isotop-filter-area')
        // if(Container.length > 0){
        //     document.querySelector('.property-filter-area').imagesLoaded(function() {
        //         var festivarMasonry = 
        //     })
        // }
        document.querySelector('.video-play-btn').magnificPopup({
            type: 'video',
            removalDelay: 260,
            mainClass: 'mfp-zoom-in',
        })
        var leftUp = '<i class="la la-angle-up"></i>';
        var rightDown = '<i class="la la-angle-down"></i>';
        var leftAngle = '<i class="fa fa-angle-left"></i>';
        var rightAngle = '<i class="fa fa-angle-right"></i>';
        var leftArrow = '<i class="fa fa-caret-left"></i>';
        var rightArrow = '<i class="fa fa-caret-right"></i>';
        
    }
    render() {
        return (
            <div>
               <div class="preloader" id="preloader">
                        <div class="preloader-inner">
                            <div class="spinner">
                                <div class="dot1"></div>
                                <div class="dot2"></div>
                            </div>
                        </div>
                    </div>
                    {/* preloader area end */}

                    {/* search Popup */}
                    <div class="search-popup" id="search-popup">
                        <form action="index.html" class="main-search search-form">
                            <input type="text" class="form-control" placeholder="Search..." />
                            <button class="submit-btn"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    {/* search Popup */}
                    <div class="body-overlay" id="body-overlay"></div>

                    {/* navbar end */}
                    <div class="ba-navbar">
                        <div class="menu-close">
                            <i class="la la-times"></i>
                        </div>
                        <div class="ba-navbar-inner">
                            <div class="thumb">
                                <img class="logo" src="assets/img/logo2.png" alt="logo" />
                            </div>
                            <p>We have carefully selected great tasting coffees from around the world.</p>
                            <ul class="social-media">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    {/* navbar end */}

                    {/* navbar start */}
                    <div class="navbar-area style-four">
                        <nav class="navbar navbar-expand-lg">
                            <div class="nav-right-part nav-right-part-desktop">
                                <a class="btn btn-transparent menu-bar" href="#"><i class="fa fa-bars"></i></a>
                            </div>
                            <div class="container nav-container">
                                <div class="responsive-mobile-menu">
                                    <button class="menu toggle-btn d-block d-lg-none" data-target="#coffee_main_menu" 
                                    aria-expanded="false" aria-label="Toggle navigation">
                                        <span class="icon-left"></span>
                                        <span class="icon-right"></span>
                                    </button>
                                </div>
                                <div class="logo">
                                    <a href="index.html"><img src="assets/img/logo.png" alt="img" /></a>
                                </div>
                                <div class="collapse navbar-collapse" id="coffee_main_menu">
                                    <ul class="navbar-nav menu-open">
                                        <li class="menu-item-has-children current-menu-item">
                                            <a href="#">Home</a>
                                            <ul class="sub-menu">
                                                <li><a href="index.html">Home 01</a></li>
                                                <li><a href="index-2.html">Home 02</a></li>
                                                <li><a href="index-3.html">Home 03</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="about.html">About Us</a></li>
                                        <li><a href="menu.html">Menu</a></li>
                                        <li><a href="blog.html">Blog</a></li>
                                        <li class="menu-item-has-children">
                                            <a href="#">Pages</a>
                                            <ul class="sub-menu">
                                                <li><a href="about.html">About</a></li>
                                                <li><a href="blog.html">Blog</a></li>
                                                <li><a href="blog-single.html">Blog Single</a></li>
                                                <li><a href="gallery.html">Gallery</a></li>
                                                <li><a href="book.html">Book</a></li>
                                                <li><a href="menu.html">Menu</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="contact.html">Contacts</a></li>
                                    </ul>
                                </div>
                                <div class="nav-right-part nav-right-part-mobile">
                                    <a class="btn btn-transparent search" id="search" href="#"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                            <div class="nav-right-part nav-right-part-desktop">
                                <a class="btn btn-transparent search" id="search" href="#"><i class="fa fa-search"></i></a>
                            </div>
                        </nav>
                    </div> 
            </div>
        )
    }
}

export default Header
