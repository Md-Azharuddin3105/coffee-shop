import React, { Component } from 'react'
import Header from './Header'
import Footer from './Footer'

export class Menu extends Component {
    state = {
        coffees: [
            {
                imgUrl: "assets/img/favorite/1.png",
                title: 'CAFFE LATTE',
                disc: 'Fresh brewed coffee and steamed milk'
            },
            {
                imgUrl: "assets/img/favorite/1.png",
                title: 'CAFFE LATTE',
                disc: 'Fresh brewed coffee and steamed milk'
            },
            {
                imgUrl: "assets/img/favorite/1.png",
                title: 'CAFFE LATTE',
                disc: 'Fresh brewed coffee and steamed milk'
            },
            {
                imgUrl: "assets/img/favorite/1.png",
                title: 'CAFFE LATTE',
                disc: 'Fresh brewed coffee and steamed milk'
            },

            {
                imgUrl: "assets/img/favorite/1.png",
                title: 'CAFFE LATTE',
                disc: 'Fresh brewed coffee and steamed milk'
            },
            {
                imgUrl: "assets/img/favorite/1.png",
                title: 'CAFFE LATTE',
                disc: 'Fresh brewed coffee and steamed milk'
            },
            {
                imgUrl: "assets/img/favorite/1.png",
                title: 'CAFFE LATTE',
                disc: 'Fresh brewed coffee and steamed milk'
            },
            {
                imgUrl: "assets/img/favorite/1.png",
                title: 'CAFFE LATTE',
                disc: 'Fresh brewed coffee and steamed milk'
            }

        ],
        navItems: [
            {
                imgUrl: "assets/img/icon/16.png" 
            },
            {
                imgUrl: "assets/img/icon/16.png" 
            },
            {
                imgUrl: "assets/img/icon/16.png" 
            },
            {
                imgUrl: "assets/img/icon/16.png" 
            }
        ]
    }
    render() {
        return (
            <div>
                <Header />
                <div class="page-title-area text-center" style="background-image: url('./assets/img/bg/17.png')">
                    <div class="container">
                        <div class="breadcrumb-inner">
                            <h2 class="page-title">Menu</h2>
                        </div>
                    </div>
                </div>
                {/* page title end */}

                {/* Menu area start */}
                <div class="menu-area menu-area-bg1 pd-top-150 pd-bottom-130" style="background-image: url('./assets/img/bg/16.png')">
                    <div class="container">
                        <div class="favorite-coffee-wrap style-three">
                            <div class="cfs-tabs-wrap text-center">
                                <ul class="nav nav-tabs cfs-tabs" id="myTab" role="tablist">
                                    {this.state.navItems.map((item) => {
                                         <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#tab_one" role="tab" aria-controls="tab_one" aria-selected="true">
                                                <img src={item.imgUrl} alt="img" />
                                            </a>
                                        </li>
                                    })}
                                </ul>
                            </div>
                            <div class="tab-content cfs-tab-content style-four" id="myTabContent">
                                <div class="tab-pane fade show active" id="tab_one" role="tabpanel">
                                    <div class="favorite-coffee-inner">
                                        <div class="row">
                                                {this.state.coffees.map((coffee) => {
                                                    <div class="col-lg-6">
                                                        <div class="single-favorite-coffee">
                                                            <div class="media">
                                                                <div class="media-left">
                                                                    <img src={coffee.imgUrl} alt="img" />
                                                                </div>
                                                                <div class="media-body align-self-center">
                                                                    <h5>{coffee.title}</h5>
                                                                    <p>{coffee.disc}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                })}
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_two" role="tabpanel">
                                    <div class="row">
                                        {this.state.coffees.map((coffee) => {
                                            <div class="col-lg-6">
                                                <div class="single-favorite-coffee">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <img src={coffee.imgUrl} alt="img" />
                                                        </div>
                                                        <div class="media-body align-self-center">
                                                            <h5>{coffee.title}</h5>
                                                            <p>{coffee.disc}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        })}
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_three" role="tabpanel">
                                    <div class="row">
                                        {this.state.coffees.map((coffee) => {
                                            <div class="col-lg-6">
                                                <div class="single-favorite-coffee">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <img src={coffee.imgUrl} alt="img" />
                                                        </div>
                                                        <div class="media-body align-self-center">
                                                            <h5>{coffee.title}</h5>
                                                            <p>{coffee.disc}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        })}
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_four" role="tabpanel">
                                    <div class="row">
                                        {this.state.coffees.map((coffee) => {
                                            <div class="col-lg-6">
                                                <div class="single-favorite-coffee">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <img src={coffee.imgUrl} alt="img" />
                                                        </div>
                                                        <div class="media-body align-self-center">
                                                            <h5>{coffee.title}</h5>
                                                            <p>{coffee.disc}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Menu area end */}

                {/* favorite area end */}
                <div class="favorite-area pd-top-105 pd-bottom-150">
                    <div class="container">
                        <div class="section-title text-center">
                            <h2 class="title">Tried These?</h2>
                            <p>THESE ARE OUR SPECIALS</p>
                        </div>
                        <div class="favorite-coffee-wrap style-two">
                            <img class="h1-favorite-coffee-img" src="assets/img/bg/10.png" alt="img" />
                            <div class="tab-content cfs-tab-content style-three">
                                <div class="tab-pane fade show active" id="tab_onez" role="tabpanel">
                                    <div class="favorite-coffee-inner">
                                        <div class="row">
                                            {this.state.coffees.map((coffee) => {
                                                <div class="col-lg-6">
                                                    <div class="single-favorite-coffee">
                                                        <div class="media">
                                                            <div class="media-left">
                                                                <img src={coffee.imgUrl} alt="img" />
                                                            </div>
                                                            <div class="media-body align-self-center">
                                                                <h5>{coffee.title}</h5>
                                                                <p>{coffee.disc}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            })}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* favorite area end */}
                <Footer />
            </div>
        )
    }
}

export default Menu
