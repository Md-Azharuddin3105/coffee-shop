// import logo from './logo.svg';
import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from './Components/Home'
import About from './Components/About'
import Menu from './Components/Menu'
import Blog from './Components/Blog'
import BlogSingle from './Components/BlogSingle'
import Gallery from './Components/Gallery'
import Book from './Components/Book'
import './App.css';

class App extends React.Component {
  render(){
    return (

        <div className="App">
          <Router>
            <Switch>
              <Route path = '/' exact component={Home} />
              <Route path = '/menu' exact component={Menu} />
              <Route path = '/about' exact component={About} />
              <Route path = '/blog' exact component={Blog} />
              <Route path = '/blogsingle' exact component={BlogSingle} />
              <Route path = '/gallery' exact component={Gallery} />
              <Route path = '/book' exact component={Book} />

            </Switch>
          </Router>
        </div>
    );
  }
}

export default App;
